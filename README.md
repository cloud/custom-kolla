# custom-kolla

## Description
In https://gitlab.ics.muni.cz/cloud/kolla is image with ceph version 14.2.9. Update of RGW to version 14.2.20 or newer was required.

Image ```registry.gitlab.ics.muni.cz:443/cloud/kolla/centos-binary-ceph-rgw``` is here under ```registry.gitlab.ics.muni.cz:443/cloud/custom-kolla/centos-binary-ceph-rgw``` with update.

Detailes in https://gitlab.ics.muni.cz/cloud/internal-wiki/-/issues/1085.
