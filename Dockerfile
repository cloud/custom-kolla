FROM registry.gitlab.ics.muni.cz:443/cloud/kolla/centos-binary-ceph-rgw:9.2.0-w30

RUN yum -y update *14.2.9* && yum clean all
